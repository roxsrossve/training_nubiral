
### ECS

resource "aws_ecs_cluster" "main" {
  name = "training"
}

data "aws_iam_role" "role"{
  name = var.task
}

resource "aws_ecs_task_definition" "app" {
  family                   = "training-task"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "${var.fargate_cpu}"
  memory                   = "${var.fargate_memory}"
  execution_role_arn       = data.aws_iam_role.role.arn
  container_definitions = jsonencode([
    {
      name      = "${var.container_name}"
      image     = "${var.app_image}"
      essential = true
      portMappings = [
        {
          containerPort = "${var.app_port}"
          hostPort      = "${var.app_port}"
        }
      ]
    }
  ])
}

  resource "aws_ecs_service" "main" {
  name            = "training-service"
  cluster         = "${aws_ecs_cluster.main.id}"
  task_definition = "${aws_ecs_task_definition.app.arn}"
  desired_count   = "${var.app_count}"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = ["${aws_security_group.ecs_tasks.id}"]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.app.id}"
    container_name   = "${var.container_name}"
    container_port   = "${var.app_port}"
  }

  depends_on = [
    "aws_alb_listener.front_end",
  ]
}
