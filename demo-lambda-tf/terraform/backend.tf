terraform {
  backend "s3" {
    bucket         = "training-gitlab-tf"
    region         = "us-east-1"
    encrypt        = true
    key            = "demo/deploy.tfstate"
    role_arn       = "arn:aws:iam::015319782619:role/training-gitlab-deploy-role"
  }
}
